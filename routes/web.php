<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/registration', function () {
    return view('registration');
})->name('registration');

Route::get('/login', function () {
    return view('login');
})->name('login');
Route::get('/logout', function () {
    Session::forget('logUser');
    Session::forget('admin');
    return view('home');
})->name('logout');

Route::get('/product/create', function () {
    return view('/Product/CreateProduct');
})->name('product-blade');

Route::get('/category/create', function () {
    return view('/Product/CreateCategory');
})->name('category-blade');

Route::post('/registration/submit','UserController@register')->name('register-form');

Route::post('/login/submit','UserController@check')->name('login-form');

Route::get('/users/get','UserController@getUsers');

Route::get('/product','ProductController@showProduct')->name('show-product');

Route::get('/product/basket','ProductController@showInBasket')->name('show-in-basket');


Route::get('/products/get','ProductController@getProduct');

Route::post('/product/addToBasket','ProductController@addToBasket');

Route::get('/product/getFromBasket','ProductController@getFromBasket');

Route::post('/product/deleteFromBasket/{productId}','ProductController@deleteFromBasket');

Route::post('/product/submit','ProductController@createProduct')->name('create-product');

Route::post('/product/change','ProductController@changeProduct');

Route::post('/product/delete/{deleteProductId}','ProductController@deleteProduct');

Route::post('/category/submit','ProductController@createCategory')->name('create-category');

Route::get('/category/get','ProductController@getCategories')->name('get-categories');

Route::post('/category/change','ProductController@changeCategories');

Route::post('/category/delete/{deleteCategoryId}','ProductController@deleteCategories');


