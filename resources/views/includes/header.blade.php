<div class="d-flex flex-column flex-md-row align-items-center pb-3 mb-4 border-bottom">
    <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
        <span class="fs-4"> My Magazine </span>
    </a>

    <nav class="d-inline-flex mt-2 mt-md-0 ms-md-auto">
        <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('home')}}">Home</a>

        <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('about')}}">About</a>

        <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('show-product')}}">Products</a>



        @if(!Session::has('logUser') and !Session::has('admin'))
        <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('login')}}">Login</a>
        @endif

        @if(Session::has('logUser'))
            <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('logout')}}">LogOut</a>
            <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('show-in-basket')}}">Basket</a>

        @endif

        @if(Session::has('admin'))
            <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('logout')}}">LogOut</a>
            <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('product-blade')}}">Create Product</a>
            <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('category-blade')}}">Create Category</a>
        @endif

    </nav>
</div>
