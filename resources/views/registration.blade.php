@extends('layouts.app')
@section('title-block') Registration @endsection

@section('content')
    <h1> Registration </h1>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

{{--    <form action="{{route('register-form')}}" method="post">--}}
        @csrf
        <div class="form-group">
            <label for="name">Enter Your Name</label>
            <input type="text" name="name" placeholder="Enter Name" id="name" class="form-control">
        </div>

        <div class="form-group">
            <label for="surname">Enter Your Surname</label>
            <input type="text" name="surname" placeholder="Enter Surname" id="surname" class="form-control">
        </div>

        <div class="form-group">
            <label for="tel">Enter Phone</label>
            <input type="number" name="tel" placeholder="Enter your phone" id="tel" class="form-control" >

        </div>

        <div class="form-group">
            <label for="email">Enter Your Email</label>
            <input type="text" name="email" placeholder="Enter Email" id="email" class="form-control">
        </div>

        <div class="form-group">
            <label for="password">Enter password</label>
            <input type="password" name="password" placeholder="Enter password" id="password" class="form-control">
        </div>

        <div class="form-group">
            <label for="repassword">Enter password again </label>
            <input type="password" name="repassword" placeholder="Enter password again" id="repassword" class="form-control">
        </div>
{{--    <button id="reg">Click me</button>--}}

        <button type="button" id="registration" class="btn btn-success "> Registration </button>

{{--    </form>--}}
<script src="/js/components/Registration.js"></script>
@endsection



