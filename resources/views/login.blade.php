@extends('layouts.app')
@section('title-block') Login @endsection
@section('content')
    <h1> Login </h1>
@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('EmailError'))
    <div class="alert alert-danger">{{session()->get('EmailError')}}</div>
    <form action="{{route('registration')}}">
    <button type="submit"  class="btn btn-success "> Registration </button>
    </form>

@endif
    @php
    Session::put('EmailError',)
    @endphp
@if(Session::has('PasswordError'))
    <div class="alert alert-danger">{{Session::get('PasswordError')}}</div>

    @php
    Session::put('PasswordError')
    @endphp
@endif

<form action="{{route('login-form')}}" method="post">
    @csrf
    <div class="form-group">
        <label for="email_login">Enter Your Email</label>
        <input type="text" name="email_login" placeholder="Enter Your Email"  class="form-control" >
    </div>
    <div class="form-group">
        <label for="password_login">Enter Your Password</label>
        <input type="password" name="password_login" placeholder="Enter Password"  class="form-control" >
    </div>
    <button type="submit" class="btn btn-success "> Login </button>
</form><br><br>
    <form action="{{route('registration')}}">
        <p>If you have not your account go to registration</p>
        <button type="submit"  class="btn btn-success "> Registration </button>
    </form>
@endsection
