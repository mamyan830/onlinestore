@extends('layouts.app')
@section('title-block') CreateCategory @endsection
@section('content')
    <h1> Create Product </h1>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{route('create-category')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="categoryName">Enter Category Name</label>
            <input type="text" name="categoryName" placeholder="Enter Category Name" id="categoryName" class="form-control">
        </div>
        <button type="submit" class="btn btn-success "> Create Category </button>

    </form><br>
    <div>
        <p>You already have this categories:</p>
        <table id="showcategories" >
        </table>
    </div><br>
<div style="display: none" id="changeName">
    <label for="newName">Enter new name </label>
    <input type="text" name="newName" id="newName" placeholder="Enter New Name" class="form-control newName" ><br>
    <button type="button" id="confirmName" class="btn btn-success" >Confirm</button>
    <button type="button" id="cancelChange" class="btn btn-success" >Cancel</button>
</div>
    <div style="display: none" id="deleteConfirm" class="deleteConfirm">
        <p>Are you sure?</p>
        <button type="button" id="confirmDelete" class="btn btn-success" >Delete</button>
        <button type="button" id="cancelDelete" class="btn btn-success" >Cancel</button>

    </div>

    <script src="/js/components/ChangeCategory.js"></script>
@endsection
