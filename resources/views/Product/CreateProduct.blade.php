@extends('layouts.app')
@section('title-block') CreateProduct @endsection
@section('content')

    <h1> Create Product </h1>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @php
    $categories=DB::table('categories')->get();
    @endphp

    <form action="{{route('create-product')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Enter Product Name</label>
            <input type="text" name="name" placeholder="Enter Name" id="name" class="form-control">
        </div>

        <div class="form-group">
            <label for="image">Enter Picture URL</label>
            <input type="text" name="image" placeholder="Enter Picture URL" id="picture" class="form-control">
        </div>
        <div class="form-group">
            <label for="category">Enter Category</label>
            <select id="category" name="category" class="form-control">
                @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach

            </select>
        </div>

        <div class="form-group">
            <label for="description">Write Description</label>
            <textarea name="description" id="description" placeholder="Description" rows="3" class="form-control" ></textarea>
        </div>

        <div class="form-group">
            <label for="price">Enter Price </label>
            <input type="number" name="price" placeholder="Enter price" id="price" class="form-control">
        </div><br>
        <button type="submit" class="btn btn-success "> Create Product </button>
    </form><br>

    <div id="changePlace" style="display: none">
        <div class="form-group">
            <label for="newName">Enter Product New Name</label>
            <input type="text" name="newName" placeholder="Enter New Name" id="newName" class="form-control">
        </div>

        <div class="form-group">
            <label for="newPicture">Enter New Picture URL</label>
            <input type="text" name="newPicture" placeholder="Enter New Picture URL" id="newPicture" class="form-control">
        </div>
        <div class="form-group">
            <label for="newCategory">Enter New Category</label>
            <select  name="newCategory" class="form-control">
                @foreach($categories as $category)
                    <option id="newCategory" value="{{$category->id}}">{{$category->name}}</option>
                @endforeach

            </select>
        </div>

        <div class="form-group">
            <label for="newDescription">Write New Description</label>
            <textarea name="newDescription" id="newDescription" placeholder="New Description" rows="3" class="form-control" ></textarea>
        </div>

        <div class="form-group">
            <label for="newPrice">Enter New Price </label>
            <input type="number" name="newPrice" placeholder="Enter New price" id="newPrice" class="form-control">
        </div><br>

        <button type="button" id="confirmChange" class="btn btn-success" >Confirm</button>
        <button type="button" id="cancelChange" class="btn btn-success" >Cancel</button>

    </div>

    <div id="showProductDeletePlace" style="display: none">
        <h2>Are you sure?</h2>
        <button type="button" id="confirmProductDelete" class="btn btn-success" >Delete</button>
        <button type="button" id="cancelProductDelete" class="btn btn-success" >Cancel</button>

    </div>

    <div id="showProducts">
        <h1>You already have this products </h1>


    </div>
    <br>
<br>

    <script src="/js/components/ChangeProduct.js"></script>

@endsection
