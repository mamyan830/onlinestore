const addToBaskets =[...document.getElementsByClassName('addToBasket')]
removeFromBaskets =[...document.getElementsByClassName('removeFromBasket')]

let userId
let productId
let deleteProductId

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

getInformation()

function getInformation() {

    $.get(`/users/get`, function (getUser, status) {
        getProducts(getUser)
    })


}

function getProducts(userData){
    $.get(`/products/get`, function (getproducts, status) {
        addToBaskets.forEach((addToBasket)=>{
            addToBasket.addEventListener('click',()=>processing(addToBasket,userData,getproducts))
        })
    })
}

function processing(button,user,products){
    userId = user.id
    products.forEach((product)=>{
        if(product.id === +button.id)
        {
            productId = product.id
        }
    })

    sendProductToBasket(userId,productId)
}

function sendProductToBasket(user,product){
    $.ajax({

        type: "POST",
        url: '/product/addToBasket',
        data: {
            userId:user,
            productId:product,
        },

        success: function (data) {
            console.log('Sax normal gnacela')

            window.location.reload(true);
        },

        error: function (request, error) {
            console.log(2222222, request, error)
        },
    });
}


getProductsFromBasket()

function getProductsFromBasket(){
    $.get(`/product/getFromBasket`, function (basketProducts, status) {
        removeFromBaskets.forEach((removeFromBaskets)=>{
            removeFromBaskets.addEventListener('click',()=>deleteProcessing(removeFromBaskets,basketProducts))
        })
    })
}

function deleteProcessing(button,products){
    products.forEach((product)=>{
        if (product.id === +button.id){
            deleteProductId = product.id
        }
    })

    deleteProductFromBasket(deleteProductId)
}

function deleteProductFromBasket(productId){

    $.ajax({

        type: "POST",
        url: `/product/deleteFromBasket/${productId}`,
        data: {},
        success: function () {
            window.location.reload(true);

        },
        error: function (request, error) {
            console.log(2222222, request, error)
        },
    });
}

