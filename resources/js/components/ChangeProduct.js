const showProducts = document.getElementById('showProducts');
const confirmChange = document.getElementById('confirmChange');
const cancelChange = document.getElementById('cancelChange');
const newName=document.getElementById('newName')
const newPicture=document.getElementById('newPicture')
const newCategory=document.getElementById('newCategory')
const newDescription=document.getElementById('newDescription')
const newPrice=document.getElementById('newPrice')
const confirmProductDelete=document.getElementById('confirmProductDelete')
const cancelProductDelete=document.getElementById('cancelProductDelete')


let arr;
let changedProductId;
let deleteProductId;


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

getProducts()

function getProducts () {
    $.get(`/products/get`, function (data, status) {
        setProducts(data);
    });
}

function setProducts(products){
    arr = products

    // console.log(arr);
    products.forEach((product)=>{

        // console.log(product.category_id +" " + product.id)
        showProducts.innerHTML += `
        <div class="card" style="width: 10rem;height: 30rem;float: left">
            <img class="card-img-top" src="${product.image}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">${product.name}</h5>
                <h5 class="card-title">${product.price}</h5>
                <p class="card-text">${product.description}</p>

                <button  class="change" className="btn btn-success" id="${product.id}" >
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         className="bi bi-pencil-square" viewBox="0 0 16 16">
                        <path
                            d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                        <path fill-rule="evenodd"
                              d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                    </svg>
                </button>

                <button type="button" class="delete" className="btn btn-success" id="${product.id}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash"
                         viewBox="0 0 16 16">
                        <path
                            d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                        <path fill-rule="evenodd"
                              d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                    </svg>
                 </button>
            </div>
        </div>
        `
        });

    const productChange = [...document.getElementsByClassName('change')];
    const productDelete = [...document.getElementsByClassName('delete')];

    productChange.forEach((elem) => {
        // console.log(productChange.id + "llllllllll")
        elem.addEventListener('click',()=>showProductChangePlace(elem));
    });
    productDelete.forEach((elem) => {
        elem.addEventListener('click',()=>showProductDeletePlace(elem));
    });
}

function showProductChangePlace(elem){
    let id = +elem.id;
    let product
    arr.forEach((item)=>{
        if (item.id === id)
        {
            product = item;
        }
    })

    $("#changePlace").show();
    changedProductId = product.id
    newName.value = product.name;
    newPicture.value = product.image;
    newCategory.value = product.category_id;
    newDescription.value = product.description;
    newPrice.value = product.price;

    // console.log(changedProductId + 'lklklklklkkooo')
}
function hideProductChangePlace(){
    $("#changePlace").hide();

}

function sendProductChangeData(changedProductId,changedProductName,changedProductPicture,changedProductCategory,changedProductDescription,changedProductPrice){

    $.ajax({

        type: "POST",
        url: '/product/change',
        data: {
            changedProductId:changedProductId,
            changedProductName:changedProductName,
            changedProductPicture:changedProductPicture,
            changedProductCategory:changedProductCategory,
            changedProductDescription:changedProductDescription,
            changedProductPrice:changedProductPrice,

        },
        success: function (data) {
            console.log('Sax normal gnacela')
            window.location.reload(true);
            console.log(data)

        },
        error: function (request, error) {
            console.log(2222222, request, error)
        },
    });

}

function showProductDeletePlace(elem){
    $("#showProductDeletePlace").show()
    deleteProductId = elem.id;

}

function sendProductDeleteData(deleteProductId){
    // console.log(deleteProductId)

    $.ajax({

        type: "POST",
        url: `/product/delete/${deleteProductId}`,
        data: {},
        success: function () {
            console.log('Sax normal gnacela');
            window.location.reload(true);

        },
        error: function (request, error) {
            console.log(2222222, request, error)
        },
    });
}

function hideProductDeletePlace(){
    $("#showProductDeletePlace").hide()
}


confirmChange.addEventListener('click',()=>sendProductChangeData(changedProductId,newName.value,newPicture.value,newCategory.value,newDescription.value,newPrice.value))
cancelChange.addEventListener('click',()=>hideProductChangePlace())
confirmProductDelete.addEventListener('click',()=>sendProductDeleteData(deleteProductId))
cancelProductDelete.addEventListener('click',()=>hideProductDeletePlace())
