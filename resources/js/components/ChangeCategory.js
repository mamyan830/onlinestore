const showCategories = document.getElementById('showcategories');
const newName = document.getElementById('newName');
const confirmChangeButton = document.getElementById('confirmName');
const cancelChangeButton = document.getElementById('cancelChange');
const confirmDelete = document.getElementById('confirmDelete');
const cancelDelete = document.getElementById('cancelDelete');

let deleteCategoryId;



$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

getCategories()

function getCategories () {
    $.get(`/category/get`, function (data, status) {
        setCategories(data);
    });
}

function setCategories(category){

    category.forEach((item) => {
        showCategories.innerHTML += `
      <tr>
        <td>${item.name}</td>
        <td>
         <button  class="change" className="btn btn-success" id="${item.id}" name="${item.name}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                     className="bi bi-pencil-square" viewBox="0 0 16 16">
                    <path
                        d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fill-rule="evenodd"
                          d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                </svg>
         </button>
         </td>
        <td>
                    <button type="button" class="delete" className="btn btn-success" id="${item.id}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash"
                     viewBox="0 0 16 16">
                    <path
                        d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                    <path fill-rule="evenodd"
                          d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                </svg>
            </button>
        </td>
      </tr>
        `
    })

    const categoryChange = [...document.getElementsByClassName('change')];
    const categoryDelete = [...document.getElementsByClassName('delete')];

    categoryChange.forEach((elem) => {
        // console.log(elem.id)
        elem.addEventListener('click',()=>showCategoryChangePlace(elem));
    });
    categoryDelete.forEach((elem) => {
        elem.addEventListener('click',()=>showCategoryDeletePlace(elem));
    });
}

function showCategoryChangePlace(elem) {
    $("#changeName").show();
    newName.value = elem["name"]
    newName.id = elem.id;
}

function hideCategoryChangePlace() {
    $("#changeName").hide();
}

function sendCategoryChangeData(changeCategoryId,changedName){

    console.log(changeCategoryId,changedName)

    $.ajax({

        type: "POST",
        url: '/category/change',
        data: {
            changeCategoryId:changeCategoryId,
            changedName:changedName
        },
        success: function () {
            window.location.reload(true);

        },
        error: function (request, error) {
            console.log(2222222, request, error)
        },
    });
}

function showCategoryDeletePlace(elem) {
    $("#deleteConfirm").show();
    deleteCategoryId = elem.id;

}

function hideCategoryDeletePlace(){
    $("#deleteConfirm").hide();
}

function sendCategoryDeleteData(deleteCategoryId){
    // console.log(deleteCategoryId + 'aaa')
    $.ajax({

        type: "POST",
        url: `/category/delete/${deleteCategoryId}`,
        data: {},
        success: function () {
            window.location.reload(true);

        },
        error: function (request, error) {
            console.log(2222222, request, error)
        },
    });
}
confirmDelete.addEventListener('click',()=>sendCategoryDeleteData(deleteCategoryId))
cancelDelete.addEventListener('click',()=>hideCategoryDeletePlace())
cancelChangeButton.addEventListener('click',()=>hideCategoryChangePlace())
confirmChangeButton.addEventListener('click',()=>sendCategoryChangeData(newName.id,newName.value))


