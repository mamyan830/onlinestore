/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*************************************************!*\
  !*** ./resources/js/components/Registration.js ***!
  \*************************************************/
var clickButton = document.getElementById('registration');
var porc = document.getElementById('porc');
clickButton.addEventListener('click', function () {
  return addUser();
});
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

function addUser() {
  var name = document.getElementById('name').value;
  var surname = document.getElementById('surname').value;
  var tel = document.getElementById('tel').value;
  var password = document.getElementById('password').value;
  var repassword = document.getElementById('repassword').value;
  var email = document.getElementById('email').value;
  $.ajax({
    type: "POST",
    url: '/registration/submit',
    data: {
      name: name,
      surname: surname,
      tel: tel,
      password: password,
      repassword: repassword,
      email: email
    },
    success: function success(response) {
      // let x=response.name;
      // porc.innerHTML += `<p>${response.name}</p>`;
      location.href = '/login';
    },
    error: function error(request, _error) {
      porc.innerHTML += "<p>You have error</p>"; // console.log(request, error)
    }
  }); // $.get(`/category/create`, function(data, status){
  //     addHtml(data)
  // });
}

function addHtml(data) {}
/******/ })()
;