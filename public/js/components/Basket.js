/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*******************************************!*\
  !*** ./resources/js/components/Basket.js ***!
  \*******************************************/
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var addToBaskets = _toConsumableArray(document.getElementsByClassName('addToBasket'));

removeFromBaskets = _toConsumableArray(document.getElementsByClassName('removeFromBasket'));
var userId;
var productId;
var deleteProductId;
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
getInformation();

function getInformation() {
  $.get("/users/get", function (getUser, status) {
    getProducts(getUser);
  });
}

function getProducts(userData) {
  $.get("/products/get", function (getproducts, status) {
    addToBaskets.forEach(function (addToBasket) {
      addToBasket.addEventListener('click', function () {
        return processing(addToBasket, userData, getproducts);
      });
    });
  });
}

function processing(button, user, products) {
  userId = user.id;
  products.forEach(function (product) {
    if (product.id === +button.id) {
      productId = product.id;
    }
  });
  sendProductToBasket(userId, productId);
}

function sendProductToBasket(user, product) {
  $.ajax({
    type: "POST",
    url: '/product/addToBasket',
    data: {
      userId: user,
      productId: product
    },
    success: function success(data) {
      console.log('Sax normal gnacela');
      window.location.reload(true);
    },
    error: function error(request, _error) {
      console.log(2222222, request, _error);
    }
  });
}

getProductsFromBasket();

function getProductsFromBasket() {
  $.get("/product/getFromBasket", function (basketProducts, status) {
    removeFromBaskets.forEach(function (removeFromBaskets) {
      removeFromBaskets.addEventListener('click', function () {
        return deleteProcessing(removeFromBaskets, basketProducts);
      });
    });
  });
}

function deleteProcessing(button, products) {
  products.forEach(function (product) {
    if (product.id === +button.id) {
      deleteProductId = product.id;
    }
  });
  deleteProductFromBasket(deleteProductId);
}

function deleteProductFromBasket(productId) {
  $.ajax({
    type: "POST",
    url: "/product/deleteFromBasket/".concat(productId),
    data: {},
    success: function success() {
      window.location.reload(true);
    },
    error: function error(request, _error2) {
      console.log(2222222, request, _error2);
    }
  });
}
/******/ })()
;