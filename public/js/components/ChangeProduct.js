/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**************************************************!*\
  !*** ./resources/js/components/ChangeProduct.js ***!
  \**************************************************/
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var showProducts = document.getElementById('showProducts');
var confirmChange = document.getElementById('confirmChange');
var cancelChange = document.getElementById('cancelChange');
var newName = document.getElementById('newName');
var newPicture = document.getElementById('newPicture');
var newCategory = document.getElementById('newCategory');
var newDescription = document.getElementById('newDescription');
var newPrice = document.getElementById('newPrice');
var confirmProductDelete = document.getElementById('confirmProductDelete');
var cancelProductDelete = document.getElementById('cancelProductDelete');
var arr;
var changedProductId;
var deleteProductId;
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
getProducts();

function getProducts() {
  $.get("/products/get", function (data, status) {
    setProducts(data);
  });
}

function setProducts(products) {
  arr = products; // console.log(arr);

  products.forEach(function (product) {
    // console.log(product.category_id +" " + product.id)
    showProducts.innerHTML += "\n        <div class=\"card\" style=\"width: 10rem;height: 30rem;float: left\">\n            <img class=\"card-img-top\" src=\"".concat(product.image, "\" alt=\"Card image cap\">\n            <div class=\"card-body\">\n                <h5 class=\"card-title\">").concat(product.name, "</h5>\n                <h5 class=\"card-title\">").concat(product.price, "</h5>\n                <p class=\"card-text\">").concat(product.description, "</p>\n\n                <button  class=\"change\" className=\"btn btn-success\" id=\"").concat(product.id, "\" >\n                    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\"\n                         className=\"bi bi-pencil-square\" viewBox=\"0 0 16 16\">\n                        <path\n                            d=\"M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z\"/>\n                        <path fill-rule=\"evenodd\"\n                              d=\"M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z\"/>\n                    </svg>\n                </button>\n\n                <button type=\"button\" class=\"delete\" className=\"btn btn-success\" id=\"").concat(product.id, "\">\n                    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" className=\"bi bi-trash\"\n                         viewBox=\"0 0 16 16\">\n                        <path\n                            d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"/>\n                        <path fill-rule=\"evenodd\"\n                              d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"/>\n                    </svg>\n                 </button>\n            </div>\n        </div>\n        ");
  });

  var productChange = _toConsumableArray(document.getElementsByClassName('change'));

  var productDelete = _toConsumableArray(document.getElementsByClassName('delete'));

  productChange.forEach(function (elem) {
    // console.log(productChange.id + "llllllllll")
    elem.addEventListener('click', function () {
      return showProductChangePlace(elem);
    });
  });
  productDelete.forEach(function (elem) {
    elem.addEventListener('click', function () {
      return showProductDeletePlace(elem);
    });
  });
}

function showProductChangePlace(elem) {
  var id = +elem.id;
  var product;
  arr.forEach(function (item) {
    if (item.id === id) {
      product = item;
    }
  });
  $("#changePlace").show();
  changedProductId = product.id;
  newName.value = product.name;
  newPicture.value = product.image;
  newCategory.value = product.category_id;
  newDescription.value = product.description;
  newPrice.value = product.price; // console.log(changedProductId + 'lklklklklkkooo')
}

function hideProductChangePlace() {
  $("#changePlace").hide();
}

function sendProductChangeData(changedProductId, changedProductName, changedProductPicture, changedProductCategory, changedProductDescription, changedProductPrice) {
  $.ajax({
    type: "POST",
    url: '/product/change',
    data: {
      changedProductId: changedProductId,
      changedProductName: changedProductName,
      changedProductPicture: changedProductPicture,
      changedProductCategory: changedProductCategory,
      changedProductDescription: changedProductDescription,
      changedProductPrice: changedProductPrice
    },
    success: function success(data) {
      console.log('Sax normal gnacela');
      window.location.reload(true);
      console.log(data);
    },
    error: function error(request, _error) {
      console.log(2222222, request, _error);
    }
  });
}

function showProductDeletePlace(elem) {
  $("#showProductDeletePlace").show();
  deleteProductId = elem.id;
}

function sendProductDeleteData(deleteProductId) {
  // console.log(deleteProductId)
  $.ajax({
    type: "POST",
    url: "/product/delete/".concat(deleteProductId),
    data: {},
    success: function success() {
      console.log('Sax normal gnacela');
      window.location.reload(true);
    },
    error: function error(request, _error2) {
      console.log(2222222, request, _error2);
    }
  });
}

function hideProductDeletePlace() {
  $("#showProductDeletePlace").hide();
}

confirmChange.addEventListener('click', function () {
  return sendProductChangeData(changedProductId, newName.value, newPicture.value, newCategory.value, newDescription.value, newPrice.value);
});
cancelChange.addEventListener('click', function () {
  return hideProductChangePlace();
});
confirmProductDelete.addEventListener('click', function () {
  return sendProductDeleteData(deleteProductId);
});
cancelProductDelete.addEventListener('click', function () {
  return hideProductDeletePlace();
});
/******/ })()
;