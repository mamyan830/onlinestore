/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!***************************************************!*\
  !*** ./resources/js/components/ChangeCategory.js ***!
  \***************************************************/
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var showCategories = document.getElementById('showcategories');
var newName = document.getElementById('newName');
var confirmChangeButton = document.getElementById('confirmName');
var cancelChangeButton = document.getElementById('cancelChange');
var confirmDelete = document.getElementById('confirmDelete');
var cancelDelete = document.getElementById('cancelDelete');
var deleteCategoryId;
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
getCategories();

function getCategories() {
  $.get("/category/get", function (data, status) {
    setCategories(data);
  });
}

function setCategories(category) {
  category.forEach(function (item) {
    showCategories.innerHTML += "\n      <tr>\n        <td>".concat(item.name, "</td>\n        <td>\n         <button  class=\"change\" className=\"btn btn-success\" id=\"").concat(item.id, "\" name=\"").concat(item.name, "\">\n                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\"\n                     className=\"bi bi-pencil-square\" viewBox=\"0 0 16 16\">\n                    <path\n                        d=\"M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z\"/>\n                    <path fill-rule=\"evenodd\"\n                          d=\"M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z\"/>\n                </svg>\n         </button>\n         </td>\n        <td>\n                    <button type=\"button\" class=\"delete\" className=\"btn btn-success\" id=\"").concat(item.id, "\">\n                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" className=\"bi bi-trash\"\n                     viewBox=\"0 0 16 16\">\n                    <path\n                        d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"/>\n                    <path fill-rule=\"evenodd\"\n                          d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"/>\n                </svg>\n            </button>\n        </td>\n      </tr>\n        ");
  });

  var categoryChange = _toConsumableArray(document.getElementsByClassName('change'));

  var categoryDelete = _toConsumableArray(document.getElementsByClassName('delete'));

  categoryChange.forEach(function (elem) {
    // console.log(elem.id)
    elem.addEventListener('click', function () {
      return showCategoryChangePlace(elem);
    });
  });
  categoryDelete.forEach(function (elem) {
    elem.addEventListener('click', function () {
      return showCategoryDeletePlace(elem);
    });
  });
}

function showCategoryChangePlace(elem) {
  $("#changeName").show();
  newName.value = elem["name"];
  newName.id = elem.id;
}

function hideCategoryChangePlace() {
  $("#changeName").hide();
}

function sendCategoryChangeData(changeCategoryId, changedName) {
  console.log(changeCategoryId, changedName);
  $.ajax({
    type: "POST",
    url: '/category/change',
    data: {
      changeCategoryId: changeCategoryId,
      changedName: changedName
    },
    success: function success() {
      window.location.reload(true);
    },
    error: function error(request, _error) {
      console.log(2222222, request, _error);
    }
  });
}

function showCategoryDeletePlace(elem) {
  $("#deleteConfirm").show();
  deleteCategoryId = elem.id;
}

function hideCategoryDeletePlace() {
  $("#deleteConfirm").hide();
}

function sendCategoryDeleteData(deleteCategoryId) {
  // console.log(deleteCategoryId + 'aaa')
  $.ajax({
    type: "POST",
    url: "/category/delete/".concat(deleteCategoryId),
    data: {},
    success: function success() {
      window.location.reload(true);
    },
    error: function error(request, _error2) {
      console.log(2222222, request, _error2);
    }
  });
}

confirmDelete.addEventListener('click', function () {
  return sendCategoryDeleteData(deleteCategoryId);
});
cancelDelete.addEventListener('click', function () {
  return hideCategoryDeletePlace();
});
cancelChangeButton.addEventListener('click', function () {
  return hideCategoryChangePlace();
});
confirmChangeButton.addEventListener('click', function () {
  return sendCategoryChangeData(newName.id, newName.value);
});
/******/ })()
;