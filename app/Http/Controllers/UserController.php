<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Service\UserService;

class UserController extends Controller
{
    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(RegisterRequest $request)
    {
        $user = [
            'name'=>$request->name,
            'surname'=>$request->surname,
            'tel' => $request->tel,
            'email' => $request->email,
            'password' => $request->password,
        ];
        return $this->userService->register($user);
//        return redirect()->route('login');

    }

    public function check (LoginRequest $request){
        $email = $request->email_login;
        $password = $request->password_login;
        return $this->userService->login($email, $password);
    }

    public function getUsers(){
        $users = $this->userService->getUsers();
        return $users;
    }
}


