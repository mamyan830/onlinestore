<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddToBasketRequest;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\ChangeCategoryRequest;
use App\Http\Requests\ChangeProductRequest;
use App\Http\Requests\DeleteCategoryRequest;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Service\ProductService;
use http\Env\Request;

class ProductController extends Controller{
    public $productService;

    public function __construct(ProductService $productService){
        $this->productService=$productService;
    }

    public function createProduct( ProductRequest $request){
        $product=[
            'name'=>$request->name,
            'image'=>$request->image,
            'category'=>$request->category,
            'description'=>$request->description,
            'price'=>$request->price,
        ];
        return $this->productService->createProduct($product) ;
    }

    // this is for testing

    public function changeProduct(ChangeProductRequest $request){
        $id = $request->changedProductId;
        $newName = $request->changedProductName;
        $newPicture = $request->changedProductPicture;
        $newCategory = $request->changedProductCategory;
        $newDescription = $request->changedProductDescription;
        $newPrice = $request->changedProductPrice;

        return $this->productService->changeProduct($id,$newName,$newPicture,$newCategory,$newDescription,$newPrice);

    }

    public function deleteProduct($deleteProductId){
        $id = $deleteProductId;
        return $this->productService->deleteProduct($id);

    }

    public function showProduct (){
        $products = $this->productService->showProduct();
        return view('Product.ShowProduct',['products'=>$products]);
    }

    public function getProduct(){
        $products=$this->productService->showProduct();
        return $products;
    }

    public function addToBasket(AddToBasketRequest $request){
        $userId = $request->userId;
        $productId = $request->productId;
        return $this->productService->addToBasket($userId,$productId);
    }

    public function showInBasket(){
        $products = $this->productService->showInBasket();
        return view('Product.BasketView',['products'=>$products]);
    }

    public function getFromBasket(){
        $products = $this->productService->showInBasket();
        return $products;
    }

    public function deleteFromBasket($productId){
        $id = $productId;
        $this->productService->deleteFromBasket($id);
    }

    public function createCategory(CategoryRequest $request){
        $this->productService->createCategory($request->categoryName);
        return back();
    }

    public function getCategories(){
        $categories = $this->productService->getCategories();
        return $categories;
    }

    public function changeCategories(ChangeCategoryRequest $request){
//        dd($request->categoryId);
        $id = $request->changeCategoryId;
        $newName=$request->changedName;
        return $this->productService->changeCategories($id,$newName);
    }

    public function deleteCategories($deleteCategoryId){
        $id = $deleteCategoryId;
        return $this->productService->deleteCategories($id);
    }

}
