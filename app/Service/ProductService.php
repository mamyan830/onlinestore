<?php

namespace App\Service;

use App\Http\Requests\ProductRequest;
use App\Models\Basket;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductService
{
    public function __construct()
    {
    }
    public function createProduct(Array $product){
        Product::create([
            'name'=>$product['name'],
            'image'=>$product['image'],
            'category'=>$product['category'],
            'description'=>$product['description'],
            'price'=>$product['price'],
        ]);
        return redirect()->route('show-product');
    }

    public function showProduct(){
        $products = Product::all();
        return $products;
    }

    public function addToBasket($userId,$productId)
    {
        $alredyHas = Basket::where('user_id', $userId)->where('product_id', $productId)->first();
        if (!$alredyHas) {
            Basket::create([
                'user_id' => $userId,
                'product_id' => $productId,
            ]);
        }
    }
    public function showInBasket(){
            $products = DB::table('products')->select('products.id','products.name','products.image','products.category_id','products.description','products.price')
                ->join('baskets','baskets.product_id','=','products.id')
                ->where('baskets.user_id',Session::get('logUser')->id)
                ->get();
            return $products;
    }

    public function deleteFromBasket($id){
        return Basket::where('product_id',$id)->where('user_id',Session::get('logUser')->id)->delete();

    }

    public function changeProduct($id,$newName,$newPicture,$newCategory,$newDescription,$newPrice){
        $products = Product::where('id',$id)->first();
        $products->name = $newName;
        $products->image = $newPicture;
        $products->category_id = $newCategory;
        $products->description = $newDescription;
        $products->price = $newPrice;
        $products->save();
        return $products;
    }

    public function deleteProduct($id){
        return Product::where('id',$id)->delete();
    }
//    public function getProducts(){
//        $products = Product::all();
//        return $products;
//    }

    public function createCategory($name){
        Category::create([
            'name' => $name,
        ]);
    }

    public function getCategories(){
        $categories = Category::all();
        return $categories;
    }

    public function changeCategories($id,$newName){
        $categories = Category::where('id',$id)
            ->first();
        $categories->name = $newName;
        $categories->save();
        return $categories;
//        DB::update('update categories set name=$newName where id=$id');
    }

    public function deleteCategories($id){
//        dd('vaxo');
        return Category::where('id',$id)->delete();
    }

}
