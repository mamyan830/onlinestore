<?php

namespace App\Service;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserService
{

    public function __construct()
    {
    }

    public function register (Array $user) {
      $user =  User::create([
            'name' => $user['name'],
            'surname'=> $user['surname'],
            'tel' => $user['tel'],
            'email' => $user['email'],
            'password' => $user['password'],
        ]);
      return $user;
    }

    public function login($email, $password)
    {
        $user=DB::table('users')->where('email',$email)->first();

        if (!$user){

            Session::put("EmailError","User not found in that email.If you are not registered go to registration");
            return redirect()->route('login');
        }

        if (Hash::check($password, $user->password)){
            if ($user->status == User::ADMIN){
                $adminUser=array('id'=>$user->id,'name'=>$user->name,'surname'=>$user->surname,'tel'=>$user->tel,'email'=>$user->email);
                $adminUser= (object)$adminUser;
                Session::put('admin',$adminUser);
                return view('about');

            } else  {
                $logUser= array('id'=>$user->id,'name'=>$user->name,'surname'=>$user->surname,'tel'=>$user->tel,'email'=>$user->email);
                $logUser=(object)$logUser;
                Session::put('logUser',$logUser);
                return view('home');
            }

        }
        else{
            Session::put("PasswordError","Password is wrong.");
            return view('login');
        }
    }

    public function getUsers(){
       $user =  Session::get('logUser');
        return $user;
    }
}
