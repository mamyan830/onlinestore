<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class User extends Model{
    const ADMIN = 1;
    const USER = 0;

    protected $fillable = [
        'name',
        'surname',
        'tel',
        'email',
        'password',
        'status'
    ];

    public static function create (Array $array) {
        $registration = new static();
        $registration->name=$array['name'];
        $registration->surname=$array['surname'];
        $registration->email=$array['email'];
        $registration->password=Hash::make($array['password']);
        $registration->tel=$array['tel'];
//        $registration->status = static::USER;
        $registration->save();
        return $registration;
    }

}
