<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    protected $fillable=[
        'user_id',
        'product_id'
    ];
    public static function create(Array $array){
        $basket = new static();
        $basket->user_id = $array['user_id'];
        $basket->product_id = $array['product_id'];
        $basket->save();
        return $basket;
    }
}
