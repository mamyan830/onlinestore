<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{
    protected $fillable=[
        'name',
        'image',
        'category_id',
        'description',
        'price',
    ];
     public static function create(Array $array){
         $product=new static();
         $product->name=$array['name'];
         $product->image=$array['image'];
         $product->category_id=$array['category'];
         $product->description=$array['description'];
         $product->price=$array['price'];
         $product->save();
         return $product;
     }

}
